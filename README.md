# City Server

Demonstrating a file client/server in C and other languages too.

Quick and dirty. 

Make it pretty later.


## Guidance and design decisions:

### Client
- Takes command line arguments for a path
- Sends "GET /path/to/my/file"
- Waits for receive message and saves it

### Server
- Persistent (runs until controller kills it)
- Listens for a "GET /path/to/my/file" message
- Reads file and sends over network back to client
