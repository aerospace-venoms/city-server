#!/usr/bin/env python3

import argparse
import socket

HOST = ''
PORT = 4200
MAX_REQ = 1024

def main():
    print("Hello, server world!")

    sock = socket.socket()
    sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1) 
    sock.bind((HOST,PORT))
    sock.listen()
    
    while True:
        conn, addr = sock.accept()
        data = conn.recv(MAX_REQ)
        request = data.decode().split()
        if "GET" != request[0]:
            conn.send("ERROR".encode())
        else:
            print("Requested File: "+request[1]) 
            request_file = open(request[1],'r')
            conn.sendall(request_file.read().encode())

if __name__ == "__main__":
    main()
