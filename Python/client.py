#!/usr/bin/env python3

import argparse
import socket

# Test with nc -l 4200


def main():
    print("Hello, client world!")
    host = "192.168.86.90"
    port = 4200

    s = socket.socket()
    s.connect((host, port))
    s.sendto("GET /tmp/file".encode(), (host, port))
    received = s.recv(1024).decode()
    s.close()

    print("Received:", received)
    with open("out", "w") as file:
        file.write(received)


if __name__ == "__main__":
    main()
